-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2016 at 02:58 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `markssystem_by_islam_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `markssystem_by_islam_batch_tb`
--

CREATE TABLE IF NOT EXISTS `markssystem_by_islam_batch_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(255) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `trainer_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `markssystem_by_islam_batch_tb`
--

INSERT INTO `markssystem_by_islam_batch_tb` (`id`, `batch_no`, `batch_name`, `trainer_name`) VALUES
(18, '1st', 'Web Development- PHP', 'Yameen'),
(19, '1st', 'Web Design', 'Sayma Sabrin');

-- --------------------------------------------------------

--
-- Table structure for table `markssystem_by_islam_exam_tb`
--

CREATE TABLE IF NOT EXISTS `markssystem_by_islam_exam_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(255) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `exam_no` varchar(255) NOT NULL,
  `exam_name` varchar(255) NOT NULL,
  `exam_date` varchar(255) NOT NULL,
  `exam_question` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `markssystem_by_islam_exam_tb`
--

INSERT INTO `markssystem_by_islam_exam_tb` (`id`, `batch_no`, `batch_name`, `exam_no`, `exam_name`, `exam_date`, `exam_question`) VALUES
(1, '1st', 'Web Design', '01', 'Web Design-01', '01/01/2016', 'Nai...');

-- --------------------------------------------------------

--
-- Table structure for table `markssystem_by_islam_trainee_tb`
--

CREATE TABLE IF NOT EXISTS `markssystem_by_islam_trainee_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(255) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `seid_no` varchar(255) NOT NULL,
  `trainee_name` varchar(255) NOT NULL,
  `trainee_email` varchar(255) NOT NULL,
  `trainee_phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `markssystem_by_islam_trainee_tb`
--

INSERT INTO `markssystem_by_islam_trainee_tb` (`id`, `batch_no`, `batch_name`, `seid_no`, `trainee_name`, `trainee_email`, `trainee_phone`) VALUES
(22, '1st', 'Web Design', '101', 'Islam Hossain', 'islam20162016@gmail.com', '+8801914441334'),
(23, '1st', 'Web Development- PHP', '108298', 'Shirin Tumpa', 'islamislam@gmail.com', '+8801700014712');

-- --------------------------------------------------------

--
-- Table structure for table `markssystem_by_islam_user_tb`
--

CREATE TABLE IF NOT EXISTS `markssystem_by_islam_user_tb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `markssystem_by_islam_user_tb`
--

INSERT INTO `markssystem_by_islam_user_tb` (`id`, `user_email`, `user_password`) VALUES
(1, 'islam@gmail.com', '123');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
