<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trainee Maintenance System</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- My selected style -->
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h1>
                  <small><img src="image/icon.png" alt="Basis_LOGO" height="20%" width="10%"></small>
                  <header>Trainee Maintenance System</header>
              </h1>
          </div>          
      </div>
      <!-- header section end -->
      <div class="container">
          <div class="content">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                  <form id="login_form" name="user_login_form" onsubmit="return validate();" action="views/SEIP106299/Admin/Login.php" method="post">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" name="user_email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" name="user_password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                 </form>
              </div><!--div 8 close-->
              
          </div><!--content close-->  
      </div>
      <!-- container section end -->
      <div class="container-fluid">
          <div class="container">
              <div class="col-md-4"><br/>Developed By-<a href="">Islam Hossain</a></div>
              <div class="col-md-4"><br/>copyright &copy 2016</div>
              <div class="col-md-4"><br/>Powered By- Basis</div>
          </div> 
      </div>
      <!-- footert section end -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
<script type="text/javascript"> 
	function validate(){
	var user_email=document.forms["user_login_form"]["user_email"].value; 	
	var user_password=document.forms["user_login_form"]["user_password"].value; 
			
	if(user_email==null || user_email==""){
		alert("Username Missing!!");
		return false;
	}else if(user_password==null || user_password==""){
		alert("Password Missing!!")
                return false;
	}
}
</script> 

  </body>
</html>
