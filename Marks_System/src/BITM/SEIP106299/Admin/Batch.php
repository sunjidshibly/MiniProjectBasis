<?php

namespace src\BITM\SEIP106299\Admin;

class Batch {
    
    public $id = "";
    public $batch_no = "";
    public $batch_name = "";
    public $trainer_name = "";
    
    public function __construct() {
        $con=  mysql_connect("localhost","root","")or die("Server not connected !");
        $db= mysql_select_db("markssystem_by_islam_db")or die("Database not Found !");
    }
    
    public function prepare($_batch = array()){
        if(is_array($_batch)&& array_key_exists('batch_no', $_batch)){
           
            $this->batch_no = $_batch['batch_no'];
            $this->batch_name = $_batch['batch_name'];
            $this->trainer_name = $_batch['trainer_name'];
            
            if($this->batch_no!=null || $this->batch_no!=""){
                if($this->batch_name!=null||$this->batch_name!=""){
                    ///////////////////////////////
                    //echo "success..";////////////
                    ///////////////////////////////
                    return $this;
                }else{
                    echo "<script>alert('Batch Name not null');</script>";
                    echo "<script>window.location='add_batch.php';</script>";
                }
            }else{
                echo "<script>alert('Batch ID/Number not null');</script>";
                echo "<script>window.location='add_batch.php';</script>";
            }
        }

    }
    public function Store_batch(){        
        $query = "SELECT `id`, `batch_no`, `batch_name`, `trainer_name` FROM `markssystem_by_islam_batch_tb` WHERE `batch_no`='".$this->batch_no."' AND `batch_name`='".$this->batch_name."'";
        $result = mysql_query($query)or die("batch check query wrong");
        if($data_batch=  mysql_fetch_array($result)){
            echo "<script>alert('Batch Already Exists');</script>";
            echo "<script>window.location='Add_batch.php';</script>";
        }else{
            $ins_query = "INSERT INTO `markssystem_by_islam_db`.`markssystem_by_islam_batch_tb` (`id`, `batch_no`, `batch_name`, `trainer_name`) VALUES (NULL, '".$this->batch_no."', '".$this->batch_name."', '".$this->trainer_name."')";
            $ins_result = mysql_query($ins_query)or die('Batch query worng');
            if($ins_result){
            echo "<script>alert('Batch Add Successfull !');</script>";
            echo "<script>window.location='Show_batch.php';</script>";
            }
        }
        
    }
    public function Show_batch(){
          $show_query ="SELECT * FROM `markssystem_by_islam_batch_tb`";
          $show_result = mysql_query($show_query)or die("show Batch query worng");
          
        $_batch = array();        

        while($row= mysql_fetch_assoc($show_result)){
            $_batch[ ] = $row; 
        }               
        return $_batch;
    }
    public function delete($id = null) {
        if(is_null($id)){
            return;
        }
        //$show_qry = "SELECT * FROM `mobile_islam_tb` WHERE ID=".$id;
        $delete_qry = "DELETE FROM `markssystem_by_islam_db`.`markssystem_by_islam_batch_tb` WHERE `markssystem_by_islam_batch_tb`.`id` =".$id;
        $result = mysql_query($delete_qry)or die("qry wrong..");
        if($result){
            echo "<script>alert('Batch Deleted!');</script>";
            echo "<script>window.location='Show_batch.php';</script>";
        }else{
            echo "<script>alert('Batch Not Deleted! Try Again');</script>";
            echo "<script>window.location='Show_batch.php';</script>";
        }
    }
    public function show($id = null) {
        
        if(is_null($id)){
            return;
        }
        $show_qry = "SELECT * FROM `markssystem_by_islam_batch_tb` WHERE ID=".$id;
        $result = mysql_query($show_qry);
        $mobile= mysql_fetch_assoc($result);
        
        $_edit_batch = array();
        $_edit_batch[]=$mobile;
        
        return $_edit_batch;
    }
    public function prepareUpdate($_batch = array()){
        if(is_array($_batch)&& array_key_exists('id', $_batch)){
           $this->id = $_batch['id'];
           $this->batch_no = $_batch['batch_no'];
           $this->batch_name = $_batch['batch_name'];
           $this->trainer_name = $_batch['trainer_name'];
        }
        return $this;
    }
    public function update() {         
       
        $save_qry = "UPDATE `markssystem_by_islam_db`.`markssystem_by_islam_batch_tb` SET `batch_no` = '".$this->batch_no."', `batch_name` = '".$this->batch_name."', `trainer_name` = '".$this->trainer_name."' WHERE `markssystem_by_islam_batch_tb`.`id` = '".$this->id."'";
        $save_flag = mysql_query($save_qry)or die("update qry wrong");
        
        if($save_flag){
            echo "<script>alert('Successfully Updated!');</script>";
            echo "<script>window.location='Show_batch.php';</script>";
        }else{
            echo "<script>alert('Not Updated!');</script>";
            echo "<script>window.location='Show_batch.php';</script>";
        }
    }
}
