<?php
    session_start();
    if(isset($_SESSION['user_email'])){       
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trainee Maintenance System</title>

    <!-- Bootstrap -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">
    <!-- My selected style -->
    <link href="../../../css/style.css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h1>
                  <small><img src="../../../image/icon.png" alt="Basis_LOGO" height="20%" width="10%"></small>
                  <header>Trainee Maintenance System</header>
                  <small id="logout_btn"><a class="btn btn-primary" href="logout.php">Logout</a></small>
              </h1>
          </div>          
      </div>
      <!-- header section end -->
      <div class="container">
          <div class="nav">
              <div class="col-md-2"></div>
              <div class="col-md-10">
              <nav>
                  <div class="col-md-2"></div>
                  <div class="col-md-2"><a class="" href="Home.php">Home</a></div>
                  <div class="col-md-2"><a class="" href="Show_batch.php">Show Batch</a></div>
                  <div class="col-md-2"><a class="" href="../Exam/Show_exam_batch.php">Exam</a></div>
                  <div class="col-md-2"><a class="" href="">Show Result</a></div>
                  <div class="col-md-2"><a class="" href="">Tutorial</a></div>                  
              </nav>
              </div>
          </div>
          <div class="content">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                 
                  <form name="add_batch_form" onsubmit="return validate();" action="Store_batch.php" method="post">
                      <center><h3>Add New Batch</h3><br/><br/></center>
                    <div class="form-group">
                      <label for="exampleInputText1">Batch ID/Number</label>
                      <input type="text" name="batch_no" class="form-control" id="exampleInputEmail1" placeholder="Enter Batch Number">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Batch Name</label>
                      <input type="text" name="batch_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Batch Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputText1">Trainer Name</sup style='color:red;'>*</sup></label>
                      <input type="text" name="trainer_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Trainer Name">
                    </div>
                    <button type="submit" class="btn btn-primary">Save New Batch</button>
                 </form>
              </div><!--div 8 close-->
              
          </div><!--content close-->  
      </div>
      <!-- container section end -->
      <div class="container-fluid">
          <div class="container">
              <div class="col-md-4"><br/>Developed By-<a href="">Islam Hossain</a></div>
              <div class="col-md-4"><br/>copyright &copy 2016</div>
              <div class="col-md-4"><br/>Powered By- Basis</div>
          </div> 
      </div>
      <!-- footert section end -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
<script type="text/javascript"> 
	function validate(){
	var batch_id=document.forms["add_batch_form"]["batch_no"].value; 	
	var batch_name=document.forms["add_batch_form"]["batch_name"].value; 
			
	if(batch_id==null || batch_id==""){
		alert("Batch ID/No Missing!!");
		return false;
	}else if(batch_name==null || batch_name==""){
		alert("Batch Name Missing!!")
                return false;
	}
}
</script> 
</body>
</html>
<?php
    
    }else{
        echo "<script>alert('You Are Not A Valid User !');</script>";
        echo "<script>window.location='../../../index.php';</script>";
    }
    
?>