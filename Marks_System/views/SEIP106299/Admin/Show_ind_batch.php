<?php

function __autoload($classname){
        $file =str_replace("\\","/",$classname);  
        include ("../../../".$file.'.php');
     }

     use src\BITM\SEIP106299\Admin\Batch;
      use src\BITM\SEIP106299\Trainee\Trainee;

     $e_batch = new Batch();
     $edit_batch = $e_batch->show($_GET['id']);

    session_start();
    if(isset($_SESSION['user_email'])){       
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trainee Maintenance System</title>

    <!-- Bootstrap -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">
    <!-- My selected style -->
    <link href="../../../css/style.css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h1>
                  <small><img src="../../../image/icon.png" alt="Basis_LOGO" height="20%" width="10%"></small>
                  <header>Trainee Maintenance System</header>
                  <small id="logout_btn"><a class="btn btn-primary" href="logout.php">Logout</a></small>
              </h1>
          </div>          
      </div>
      <!-- header section end -->
      <div class="container">
          <div class="nav">
              <div class="col-md-2"></div>
              <div class="col-md-10">
              <nav>
                  <div class="col-md-2"></div>
                  <div class="col-md-2"><a class="" href="Home.php">Home</a></div>
                  <div class="col-md-2"><a class="" href="Show_batch.php">Show Batch</a></div>
                  <div class="col-md-2"><a class="" href="">Add Exam</a></div>
                  <div class="col-md-2"><a class="" href="">Show Result</a></div>
                  <div class="col-md-2"><a class="" href="">Tutorial</a></div>                  
              </nav>
              </div>
          </div>
          <div class="content">
<!----------------------------------------->
<!----------------------------------------->
<!----------------------------------------->
<div class="col-md-3" style="background-color:#EFEEEC;">
    
 <?php 
$space = "&nbsp &nbsp";
foreach ($edit_batch as $e_batch): 
?>
    <table class="table table-bordered">
        <tbody>            
            <tr><th id="ind_show">Batch ID</th><td><?php echo $space.$e_batch['batch_no']; ?></td></tr>
            <tr><th id="ind_show">Batch Name</th><td><?php echo $space.$e_batch['batch_name']; ?></td></tr>
            <tr><th id="ind_show">Trainer Name</th><td><?php echo $space.$e_batch['trainer_name']; ?></td></tr>        
        </tbody>
    </table>

    <div class="add_trainee_form">
        <form name="add_trainee_form" onsubmit="return validate();" action="../Trainee/Trainee_save.php" method="post">
                      <center><h3><u>Add New Trainee</u></h3></center>
                      <input type="hidden" name="id" value="<?php echo $e_batch['id']; ?>">
                      <input type="hidden" name="batch_no" value="<?php echo $e_batch['batch_no']; ?>">
                      <input type="hidden" name="batch_name" value="<?php echo $e_batch['batch_name']; ?>">
                    <div class="form-group">
                      <label for="exampleInputText1">SEIP ID</label>
                      <input type="text" name="seid_no" class="form-control" id="exampleInputEmail1" placeholder="Enter SEID No">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Name</label>
                      <input type="text" name="trainee_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Email</label>
                      <input type="email" name="trainee_email" class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Phone/Mobile</label>
                      <input type="text" name="trainee_phone" class="form-control" id="exampleInputEmail1" placeholder="Enter Phone/Mobile No">
                    </div>
                    <button type="submit" class="btn btn-primary">Save New Trainee</button>
            </form>
        <br/>
    </div>
</div>

<div class="col-md-9">
    
 <?php 
 
 
    
    $trainee = new Trainee();
    $_trainee = $trainee->Show_trainee($e_batch['batch_no'],$e_batch['batch_name']);
    //$edit_batch = $e_batch->show($_GET['id']);
    
    


          
?>
     <div class="table-responsive"> 
     <table class="table table-bordered">
         <thead>
                        <tr>
                            <th>SEIP ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Action</th>
                        </tr>
         </thead>

         <tbody>
<?php  
         foreach ($_trainee as $trainee):                
?>
             <tr>
                 <td><a id="action" href=""><?php echo $trainee['seid_no']; ?></a></td>
                 <td><a id="action" href=""><?php echo $trainee['trainee_name']; ?></a></td>
                 <td><a id="action" href=""><p><?php echo $trainee['trainee_email']; ?></p></a></td>
                 <td><a id="action" href=""><?php echo $trainee['trainee_phone']; ?></a></td>
                 <td>
                     <a href="../Trainee/Trainee_edit.php?id=<?php echo $trainee['seid_no']; ?>&batch_no=<?php echo $e_batch['id']; ?>" class="btn btn-primary">Edit</a>
                     <a href="../Trainee/Trainee_delete.php?id=<?php echo $trainee['seid_no']; ?>&batch_no=<?php echo $e_batch['id']; ?>" class="btn btn-danger">Delete</a>                     
                 </td>
             </tr>
         </tbody>  
 <?php endforeach;  ?>
                </table>
         </div> 
    
<?php        
 endforeach; 
 ?>    
</div>
<!----------------------------------------->
<!----------------------------------------->
<!----------------------------------------->              
  <br/>
          </div><!--content close-->  
      </div>
      <!-- container section end -->
      <div class="container-fluid">
          <div class="container">
              <div class="col-md-4"><br/>Developed By-<a href="">Islam Hossain</a></div>
              <div class="col-md-4"><br/>copyright &copy 2016</div>
              <div class="col-md-4"><br/>Powered By- Basis</div>
          </div> 
      </div>
      <!-- footert section end -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
<script type="text/javascript"> 
	function validate(){
	var seid_no=document.forms["add_trainee_form"]["seid_no"].value; 	
	var name=document.forms["add_trainee_form"]["trainee_name"].value; 	
	var email=document.forms["add_trainee_form"]["trainee_email"].value; 	
	var phone=document.forms["add_trainee_form"]["trainee_phone"].value; 
			
	if(seid_no==null || seid_no==""){
		alert("SEID No Missing!!");
		return false;
	}else if(name==null || name==""){
		alert("Trainee Name Missing!!")
                return false;	
	}else if(email==null || email==""){
		alert("Trainee Email Missing!!")
                return false;
	}else if(phone==null || phone==""){
		alert("Trainee Phone No Missing!!")
                return false;
	}
}
</script> 
</body>
</html>
<?php
    
    }else{
        echo "<script>alert('You Are Not A Valid User !');</script>";
        echo "<script>window.location='../../../index.php';</script>";
    }
    
?>       