<?php

function __autoload($classname){
        $file =str_replace("\\","/",$classname);  
        include ("../../../".$file.'.php');
     }

     use src\BITM\SEIP106299\Admin\Batch;
     use src\BITM\SEIP106299\Trainee\Trainee;

     $e_batch = new Batch();
     $edit_batch = $e_batch->show($_GET['id']);

    session_start();
    if(isset($_SESSION['user_email'])){       
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Trainee Maintenance System</title>

    <!-- Bootstrap -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">
    <!-- My selected style -->
    <link href="../../../css/style.css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h1>
                  <small><img src="../../../image/icon.png" alt="Basis_LOGO" height="20%" width="10%"></small>
                  <header>Trainee Maintenance System</header>
                  <small id="logout_btn"><a class="btn btn-primary" href="logout.php">Logout</a></small>
              </h1>
          </div>          
      </div>
      <!-- header section end -->
      <div class="container">
          <div class="nav">
              <div class="col-md-2"></div>
              <div class="col-md-10">
              <nav>
                  <div class="col-md-2"></div>
                  <div class="col-md-2"><a class="" href="../Admin/Home.php">Home</a></div>
                  <div class="col-md-2"><a class="" href="../Admin/Show_batch.php">Show Batch</a></div>
                  <div class="col-md-2"><a class="" href="">Add Exam</a></div>
                  <div class="col-md-2"><a class="" href="">Show Result</a></div>
                  <div class="col-md-2"><a class="" href="">Tutorial</a></div>                  
              </nav>
              </div>
          </div>
          <div class="content">
<!----------------------------------------->
<!----------------------------------------->
<!----------------------------------------->
<div class="col-md-2"></div>
<div class="col-md-8" style="background-color:#EFEEEC;">
    
 <?php 
$space = "&nbsp &nbsp";
foreach ($edit_batch as $e_batch): 
    
    // echo $_GET['id']."<br/>";
     //echo $_GET['batch_no'];
     $show_trainee_upd = new Trainee();
     $_show_trainee = $show_trainee_upd->ind_update_show($_GET['id']);
     foreach ($_show_trainee as $show_trainee_upd):
?>


    <div class="add_trainee_form">
        <form name="add_trainee_form" onsubmit="return validate();" action="Trainee_update.php" method="post">
                      <center><h3><u>Update Trainee Information</u></h3></center>
                      <input type="hidden" name="batch_id" value="<?php echo $_GET['batch_no']; ?>">                                            
                      <input type="hidden" name="trainee_id" value="<?php echo $show_trainee_upd['id']; ?>">
                    <div class="form-group">
                      <label for="exampleInputText1">SEIP ID</label>
                      <input type="text" name="seid_no" value="<?php echo $show_trainee_upd['seid_no']; ?>" class="form-control" id="exampleInputEmail1" >
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Name</label>
                      <input type="text" name="trainee_name" value="<?php echo $show_trainee_upd['trainee_name']; ?>" class="form-control" id="exampleInputEmail1" >
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Email</label>
                      <input type="email" name="trainee_email" value="<?php echo $show_trainee_upd['trainee_email']; ?>" class="form-control" id="exampleInputEmail1" >
                    </div>
                    <div class="form-group">
                      <label for="exampleInputText1">Phone/Mobile</label>
                      <input type="text" name="trainee_phone" value="<?php echo $show_trainee_upd['trainee_phone']; ?>" class="form-control" id="exampleInputEmail1" >
                    </div>
                    <button  type="submit" class="btn btn-primary">Save New Trainee</button>
                    <a class="btn btn-danger" href="../Admin/Show_ind_batch.php?id=<?php echo $_GET['batch_no']; ?>">Cancel</a>
            </form>
        <br/>
    </div>
</div>

<div class="col-md-8">
    
 <?php 
     endforeach;
 endforeach; 
 ?>    
</div>
<!----------------------------------------->
<!----------------------------------------->
<!----------------------------------------->              
  <br/>
          </div><!--content close-->  
      </div>
      <!-- container section end -->
      <div class="container-fluid">
          <div class="container">
              <div class="col-md-4"><br/>Developed By-<a href="">Islam Hossain</a></div>
              <div class="col-md-4"><br/>copyright &copy 2016</div>
              <div class="col-md-4"><br/>Powered By- Basis</div>
          </div> 
      </div>
      <!-- footert section end -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
<script type="text/javascript"> 
	function validate(){
	var seid_no=document.forms["add_trainee_form"]["seid_no"].value; 	
	var name=document.forms["add_trainee_form"]["trainee_name"].value; 	
	var email=document.forms["add_trainee_form"]["trainee_email"].value; 	
	var phone=document.forms["add_trainee_form"]["trainee_phone"].value; 
			
	if(seid_no==null || seid_no==""){
		alert("SEID No Missing!!");
		return false;
	}else if(name==null || name==""){
		alert("Trainee Name Missing!!")
                return false;	
	}else if(email==null || email==""){
		alert("Trainee Email Missing!!")
                return false;
	}else if(phone==null || phone==""){
		alert("Trainee Phone No Missing!!")
                return false;
	}
}
</script> 
</body>
</html>
<?php
    
    }else{
        echo "<script>alert('You Are Not A Valid User !');</script>";
        echo "<script>window.location='../../../index.php';</script>";
    }
    
?>       