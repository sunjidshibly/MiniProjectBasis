<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!--mystyle add-->
    <link href="css/style.css" type="text/css" rel="stylesheet">

  </head>
  <body>
      <div class="container-fluid">
          <div class="container">
              <h2>
                  Phone Book Project<br/><small>ID:106299</small>
                  <a href='view/create.php'><button class='btn btn-danger'>Add New </button></a>
              </h2>
          </div>
      </div>
      <!--header end-->
      <div class="container">
          <div class="content">  
               <section>                     
                    <div class="table-responsive">
                        <center><h3>Phone Book</h3></center><br/>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Name</th>
                                    <th>Phone No</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        <?php
                          
                            function __autoload($className){
                            $filename = str_replace("\\","/",$className);
                            include_once ($filename.".php");
                            }

                            use src\Phone;

                            $object=new Phone();
                            $phone= $object->index();
                            $i=1;
                            foreach ($phone as $phno):
                        ?>
                    <tbody>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $phno['name'];?></td>                            
                            <td> <?php  echo implode(", ",unserialize($phno['phoneno']));?></td>
                            <td><?php echo $phno['address'];?></td>
                            <td> <a class="btn btn-success" href="view/view.php?id=<?Php echo $phno['id'];?>">View</a> 
                                <a  class="btn btn-primary" href="view/edit.php?id=<?Php echo $phno['id'];?>">Edit</a>                                 
                                <a class="btn btn-danger"  href="view/delete.php?id=<?Php echo $phno['id'];?>">Delete</a></td>
                        </tr>
                    </tbody>
                    <?php    
                    endforeach;
                        ?>

                </table>

      </div>
      </section>
      </div>
      </div>
      <!--content end-->
      <div class="container-fluid">
          <div class="container">
              <div class="header">
                  <div class='col-md-4'>
                      <br/>
                      <p id='left'>copyright&copy2016</p>
                  </div>
                  <div class='col-md-4'>
                      <br/><center><img src='img/icon.png' alt='icon_file_name' height="100%" width='100%'></center>
                  </div>
                  <div class='col-md-4'>
                      <br/>
                      <p id='right'>Powered By-BITM(Basis)<br/>
                             Developed By- Islam Hossain</p>
                  </div>
              </div>
          </div>
      </div>
      <!--footer end-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
